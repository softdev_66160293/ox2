package com.mycompany.ox2;

import java.util.Scanner;

public class OX2 {

    public static String player = "O";
    public static boolean gameover = false;
    public static Scanner sc = new Scanner(System.in);
    public static String[][] table = new String[3][3];
    
    public static void main(String[] args) {
        System.out.println("Welcome to OX Game!");
        while (!gameover) {
            showTable();
            switchTurn();
            inputRowCol();
            checkWin();
            checkDraw();
        }
    }
    
    public static void showTable(){
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                if (table[i][j] == null){
                    table[i][j] = "-";
                }
                System.out.print(table[i][j]+" ");
            }
                System.out.println("");
        }
    }
    
    public static void switchTurn(){
        player = (player.equals("X"))? "O" : "X";
        System.out.println("Turn " + player);
    }
    
    public static void inputRowCol(){
        boolean input = false;
        while (!input){
            System.out.print("Please input (row, col) : ");
            String rowcol = sc.nextLine();
            if (rowcol.matches("\\d+ \\d+")){
                String[] split = rowcol.split(" ");
                int row = Integer.parseInt(split[0]);
                int col = Integer.parseInt(split[1]);
                if (row >= 1 && row <= 3 && col >= 1 && col <= 3){
                    row = row-1;
                    col = col-1;
                    if (table[row][col].equals("-")){
                        table[row][col] = player;
                        input = true;
                    }
                    else {
                        System.out.println("Please try in another space");
                        continue;
                    }

                }
                else {
                    System.out.println("Please input (Number1-3, ,Number1-3)");
                    continue;
                }
            }
            else {
                System.out.println("Please input (Number, ,Number)");
                continue;
            }
        }
    }
    
    public static void checkWin(){
        for (int i=0; i<3; i++){
            if (table[i][0].equals(player) && table[i][1].equals(player) && table[i][2].equals(player)){
                showTable();
                System.out.println(" Player " + player + " Win!!!");
                gameover = true;
            }
            if (table[0][i].equals(player) && table[1][i].equals(player) && table[2][i].equals(player)){
                showTable();
                System.out.println(" Player " + player + " Win!!!");
                gameover = true;
            }
        }
        if (table[0][0].equals(player) && table[1][1].equals(player) && table[2][2].equals(player)){
            showTable();
            System.out.println(" Player " + player + " Win!!!");
            gameover = true;
        }
        if (table[0][2].equals(player) && table[1][1].equals(player) && table[2][0].equals(player)){
            showTable();
            System.out.println(" Player " + player + " Win!!!");
            gameover = true;
        }
    }
    
    public static void checkDraw(){
        boolean found = false;
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                if (table[i][j].equals("-")){
                    found = true;
                }
            }
        }
        if (found == false){
            showTable();
            System.out.println("Draw!!!");
            gameover = true;
        }
    }
}
